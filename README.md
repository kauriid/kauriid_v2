# Kauri indy-agent

This repository is a fork of the [github.com/hyperledger/indy-agent](https://github.com/hyperledger/indy-agent) node.js reference agent. 

It has been modified for use as a Kauri project proof of technology demo.

## Prerequisites

- [Docker](https://docs.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

## Usage

Build images.
```
docker-compose build
```

Start nodes, agents, and web server.
```
docker-compose up
```

Open browser.
```
open http://localhost
```

## Quick start

Agents are configured in [docker-compose.yml]() as a service with the following environment variables. Defaults found in [.env]() file.

- `PORT` Service port i.e. 3000
- `PUBLIC_DID_ENDPOINT` Public endpoint for other agents to communicate with i.e. 173.17.0.150:3000 
- `DOCKERHOST` Docker host ip (defaulted to localhost, change when not running native docker i.e. windows)
- `RUST_LOG` Rust logging level for Indy SDK i.e. DEBUG
- `TEST_POOL_IP` Node pool ip, see `pool` service i.e. 173.17.0.100
- `NAME` Value for Agent-ID.name i.e. Faber College
- `EMAIL` Value for Agent-ID.email i.e. faber@example.com
- `USERNAME` Username for UI i.e. faber
- `PASSWORD` Password for UI
- `SCHEMA_TEMPLATE_NAME` Name of schema template i.e. Transcript
- `SCHEMA_TEMPLATE_ATTRIBUTES` Attributes of schema template i.e. name,degree,status,year

The schema template isn't created, it's templated into the UI for convenience. Upon creation of a schema a proof request template is generated in the same name for each issuer i.e. `Transcript-Data`.

### Establish connection

Before two agents can communicate a relationship must be established. 

Pick any two and open in separate tabs.

**Agent 1**
- Copy **My Endpoint DID** at bottom of page.

**Agent 2**
- Click **Relationships**, **Send New Connection Request**, then paste copied DID from agent 1.

A pairwise connection has now been established between two agents. An initial proof request is automatically sent to identify the connection by asking for their name.

**Agent 1**
- Click **Messages** (refresh) and accept proof request.

**Agent 2**
- Click **Messages** (refresh) and accept proof request.
- Click **Relationships** (refresh) and see agent's name is displayed.

### Issue credential

Ensure agents have established a connection.

**Agent 1**
- Click **Issuing**
- **Create Schema** by clicking **Submit**.
- **Create Credential Definition** by clicking **Submit**.
- **Send Credential Offer**, select relationship and credential definition, click **Submit**.

**Agent 2**
- Click **Messages** (refresh) and accept credential offer.
- Accept the credential offer.
- Click **Credentials** (refresh) to see it.

### Request proof

Ensure agents have established a connection. Demo provides a proof request template for each issuing agent (Agent 1).

**Agent 1**
- Click **Proof Requests**, select relationship and template proof request, click **Submit**.

**Agent 2**
- Click **Messages** (refresh) and accept proof request.

**Agent 1**
- Click **Relationships** (refresh), select Agent 1, and see new proof. Click to validate.

## Limitations

Known limitations and bugs.

- Validation of automated general identity proof fails.
- Credentials fields are filled with dummy values.
- UI needs to be refreshed often to pick up changes from server.